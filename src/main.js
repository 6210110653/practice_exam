const bodyParser = require('body-parser')
const express = require('express')
const app = express()
const port = 8000

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())
app.use('/static', express.static('public'))

app.get('/', (req, res) => {
    res.send('Hello World!')
    
})

var a = 0;

app.post('/api/increase', (req, res) => {
    a = parseInt(req.body.a)
    a = a+1
    res.send({a})
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})